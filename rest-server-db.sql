--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.4
-- Dumped by pg_dump version 9.1.4
-- Started on 2012-07-20 05:43:33 PHT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 162 (class 3079 OID 12549)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2765 (class 0 OID 0)
-- Dependencies: 162
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 161 (class 1259 OID 24600)
-- Dependencies: 2754 2755 2756 5
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(32),
    password character varying(32),
    enabled integer DEFAULT 0,
    is_login integer DEFAULT 0,
    login_error_count integer DEFAULT 0,
    create_time timestamp without time zone,
    update_time timestamp without time zone,
    last_login_time timestamp without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 2759 (class 0 OID 24600)
-- Dependencies: 161
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, username, password, enabled, is_login, login_error_count, create_time, update_time, last_login_time) FROM stdin;
1	romeom	test123	1	0	0	\N	\N	2012-07-14 21:28:56.868
2	grace	test123	1	0	0	\N	\N	2012-07-20 04:46:53.332
\.


--
-- TOC entry 2758 (class 2606 OID 24607)
-- Dependencies: 161 161
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2764 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2012-07-20 05:43:34 PHT

--
-- PostgreSQL database dump complete
--

