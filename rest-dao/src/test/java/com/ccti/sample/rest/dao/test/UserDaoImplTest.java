package com.ccti.sample.rest.dao.test;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.ccti.sample.rest.dao.UserDao;
import com.ccti.sample.rest.dao.impl.BasicDaoSupport;
import com.ccti.sample.rest.model.User;

import junit.framework.Assert;
import junit.framework.TestCase;

public class UserDaoImplTest extends TestCase {

	private UserDao userDao;
	private BasicDaoSupport basicDaoSupport;
	
	@Override
	protected void setUp() throws Exception {
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("rest-dao.xml");
		userDao = applicationContext.getBean(UserDao.class);
		basicDaoSupport = applicationContext.getBean(BasicDaoSupport.class);
	}
	
	public void testStoreUser() {
		User user = new User();
		user.setUsername("romeom");
		user.setPassword("test123");
		userDao.save(user);
	}

	public void testGetUser() {
		testStoreUser();
		User user = userDao.getUser("romeom");
		Assert.assertNotNull(user);
	}
	
	public void testGetNullUser() {
		User user = userDao.getUser("gpam");
		Assert.assertNull(user);
	}
	
	public void testGetNullUserbyNullValue() {
		User user = userDao.getUser(null);
		Assert.assertNull(user);
	}
	
	@Override
	protected void tearDown() throws Exception {
		basicDaoSupport.getHibernateTemplate().execute(new HibernateCallback<Object>() {

			@Override
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				session.createQuery("delete from User").executeUpdate();
				return null;
			}
			
		});
	}
	
}
