package com.ccti.sample.rest.dao;

import com.ccti.sample.rest.model.User;

public interface UserDao {

	User getUser(String username);
	
	void save(User user);
	
	void update(User user);
	
}
