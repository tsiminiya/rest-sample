package com.ccti.sample.rest.dao.impl;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.ccti.sample.rest.dao.UserDao;
import com.ccti.sample.rest.model.User;

public class UserDaoImpl extends BasicDaoSupport implements UserDao {

	@Override
	public User getUser(final String username) {
		return getHibernateTemplate().execute(new HibernateCallback<User>() {

			@Override
			public User doInHibernate(Session session) throws HibernateException, SQLException {
				return (User) session.createQuery("from User where username = :username")
					.setParameter("username", username)
					.uniqueResult();
			}
			
		});
	}

	@Override
	public void save(User user) {
		getHibernateTemplate().save(user);
	}
	
	@Override
	public void update(User user) {
		getHibernateTemplate().update(user);
	}

}
