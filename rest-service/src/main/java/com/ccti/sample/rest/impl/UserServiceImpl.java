package com.ccti.sample.rest.impl;

import java.sql.Timestamp;

import com.ccti.sample.rest.UserService;
import com.ccti.sample.rest.dao.UserDao;
import com.ccti.sample.rest.model.User;

public class UserServiceImpl implements UserService {

	private UserDao userDao;
	
	@Override
	public int login(User user) {
		User foundUser = userDao.getUser(user.getUsername());
		if (foundUser == null) {
			return LOG_IN_FAILED;
		} else {
			if (user.getPassword() != null) {
				if (user.getPassword().equalsIgnoreCase(foundUser.getPassword())) {
					foundUser.setIsLogin(1);
					foundUser.setLoginErrorCount(0);
					foundUser.setLastLoginTime(new Timestamp(System.currentTimeMillis()));
					userDao.update(foundUser);
					return LOG_IN_SUCCESSFUL;
				}
			}
		}
		foundUser.setLoginErrorCount(foundUser.getLoginErrorCount() + 1);
		userDao.update(foundUser);
		return LOG_IN_FAILED;
	}

	@Override
	public boolean logout(User user) {
		User foundUser = userDao.getUser(user.getUsername());
		if (foundUser != null) {
			foundUser.setIsLogin(0);
			userDao.update(foundUser);
			return true;
		}
		return false;
	}

	public UserDao getUserDao() {
		return userDao;
	}
	
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public void save(User user) {
		userDao.save(user);
	}
	
	@Override
	public User getUser(String username) {
		return userDao.getUser(username);
	}
	
	@Override
	public boolean isUsernameAvailable(String username) {
		User user = getUser(username);
		return user == null;
	}
	
}
