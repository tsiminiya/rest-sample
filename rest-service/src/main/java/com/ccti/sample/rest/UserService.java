package com.ccti.sample.rest;

import com.ccti.sample.rest.model.User;

public interface UserService {

	int LOG_IN_SUCCESSFUL = 1;
	
	int LOG_IN_FAILED = 2;
	
	int login(User user);
	
	boolean logout(User user);
	
	void save(User user);
	
	User getUser(String username);
	
	boolean isUsernameAvailable(String username);
	
}
