package com.ccti.sample.rest.web.resource;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

@Controller
@RequestMapping("/helloWorld")
public class HelloWorldWebResource {

	/*
	 * request: http://localhost:8888/rest/web/resource/helloWorld
	 * 
	 * response: {"message":"Hello World !!!"}
	 * 
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView sayHello() {
		MappingJacksonJsonView modelAndView = new MappingJacksonJsonView();
		modelAndView.addStaticAttribute("message", "Hello World !!!");
		return new ModelAndView(modelAndView);
	}
	
	/*
	 * request: http://localhost:8888/rest/web/resource/helloWorld/addressed
	 * 
	 * response: {"to":"Robert Di","message":"Hello World !!!"}
	 * 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/addressed")
	public ModelAndView sayHelloTo() {
		MappingJacksonJsonView modelAndView = new MappingJacksonJsonView();
		Map<String, String> map = new HashMap<String, String>();
		map.put("message", "Hello World !!!");
		map.put("to", "Robert Di");
		modelAndView.setAttributesMap(map);
		return new ModelAndView(modelAndView);
	}

	/*
	 * request: http://localhost:8888/rest/web/resource/helloWorld/addressedObject
	 * 
	 * response: {"request":{"to":"Regine","message":"Hello World !!!"}}
	 * 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/addressedObject")
	public ModelAndView sayHelloToObject() {
		MappingJacksonJsonView modelAndView = new MappingJacksonJsonView();
		Message message = new Message();
		message.setMessage("Hello World !!!");
		message.setTo("Regine");
		modelAndView.addStaticAttribute("request", message);
		return new ModelAndView(modelAndView);
	}
	
	/*
	 * request: http://localhost:8888/rest/web/resource/helloWorld/addressedParam?name=Romeo+H+Maranan+Jr
	 * 
	 * response: {"to":"Romeo H Maranan Jr","message":"Hello World !!!"}
	 * 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/addressedParam")
	public ModelAndView sayHelloTo(@RequestParam("name") String name) {
		MappingJacksonJsonView modelAndView = new MappingJacksonJsonView();
		Map<String, String> map = new HashMap<String, String>();
		map.put("message", "Hello World !!!");
		map.put("to", name);
		modelAndView.setAttributesMap(map);
		return new ModelAndView(modelAndView);
	}
	
	/*
	 * request: http://localhost:8888/rest/web/resource/helloWorld/addressedPath/name/romeom/language/tagalog
	 * 
	 * response: {"to":"romeom","message":"Kumusta Mundo !!!"}
	 * 
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/addressedPath/name/{name}/language/{language}")
	public ModelAndView sayHelloToInUrl(@PathVariable("name") String name, @PathVariable("language") String language) {
		MappingJacksonJsonView modelAndView = new MappingJacksonJsonView();
		Map<String, String> map = new HashMap<String, String>();
		if (language.equalsIgnoreCase("English")) {
			map.put("message", "Hello World !!!");
		} else if (language.equalsIgnoreCase("Tagalog")) {
			map.put("message", "Kumusta Mundo !!!");
		} else {
			map.put("error", "No such language.");
		}
		map.put("to", name);
		modelAndView.setAttributesMap(map);
		return new ModelAndView(modelAndView);
	}
	
	/*
	 * Test at sayHello.html
	 * 
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/more")
	public ModelAndView sayHelloToAndMoreWords(@RequestParam("message") String message, @RequestParam("name") String name) {
		MappingJacksonJsonView modelAndView = new MappingJacksonJsonView();
		Map<String, String> map = new HashMap<String, String>();
		map.put("message", message);
		map.put("to", name);
		modelAndView.setAttributesMap(map);
		return new ModelAndView(modelAndView);
	}
	
	class Message {
		
		private String to;
		private String message;
		
		public String getMessage() {
			return message;
		}
		
		public void setMessage(String message) {
			this.message = message;
		}
		
		public String getTo() {
			return to;
		}
		
		public void setTo(String to) {
			this.to = to;
		}
		
	}
	
}
