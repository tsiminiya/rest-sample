package com.ccti.sample.rest.web.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import com.ccti.sample.rest.UserService;
import com.ccti.sample.rest.model.User;

@Controller
@RequestMapping("/user")
public class UserWebResource {

	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/create")
	public ModelAndView createUser(@RequestParam("name") final String username, @RequestParam("passwd") final String password) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setEnabled(1);
		userService.save(user);
		User foundUser = userService.getUser(username);
		MappingJacksonJsonView mappingJacksonJsonView = new MappingJacksonJsonView();
		mappingJacksonJsonView.addStaticAttribute("user-id", foundUser.getId());
		return new ModelAndView(mappingJacksonJsonView);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/available")
	public ModelAndView isUserAvailable(@RequestParam("name") final String username) {
		boolean isUserAvailable = userService.isUsernameAvailable(username);
		MappingJacksonJsonView mappingJacksonJsonView = new MappingJacksonJsonView();
		mappingJacksonJsonView.addStaticAttribute("username", username);
		mappingJacksonJsonView.addStaticAttribute("available", isUserAvailable);
		return new ModelAndView(mappingJacksonJsonView);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/record")
	public ModelAndView getUser(@RequestParam("name") final String username) {
		User user = userService.getUser(username);
		MappingJacksonJsonView mappingJacksonJsonView = new MappingJacksonJsonView();
		mappingJacksonJsonView.addStaticAttribute("user", user);
		return new ModelAndView(mappingJacksonJsonView);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/login")
	public ModelAndView login(@RequestParam("name") final String username, @RequestParam("passwd") final String password) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		int loginResult = userService.login(user);
		MappingJacksonJsonView mappingJacksonJsonView = new MappingJacksonJsonView();
		mappingJacksonJsonView.addStaticAttribute("login-result", loginResult);
		return new ModelAndView(mappingJacksonJsonView);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/logout")
	public ModelAndView logout(@RequestParam("name") final String username) {
		User user = new User();
		user.setUsername(username);
		MappingJacksonJsonView mappingJacksonJsonView = new MappingJacksonJsonView();
		mappingJacksonJsonView.addStaticAttribute("logged-out", userService.logout(user));
		return new ModelAndView(mappingJacksonJsonView);
	}
	
}
