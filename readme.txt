To run the program, create a PostgreSQL database named, rest-server-db.
And then, execute the following:

	psql -U postgres -f rest-server-db.sql rest-server-db

This will create the contents of the database.
